import xlwt

book=xlwt.Workbook(encoding="utf-8")
sheet=book.add_sheet("9x9乘法表",cell_overwrite_ok=True)
for i in range(1,10):
    for j in range(1,10):
        s=str(i)+" x "+str(j)+" = "+str(i*j)

        sheet.write(i-1,j-1,s)
book.save("9x9乘法表.xls")
