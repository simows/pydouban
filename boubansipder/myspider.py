
from bs4 import BeautifulSoup     #网页解析，获取数据
import re       #正则表达式，进行文字匹配
import urllib.request,urllib.error      #制定URL，获取网页数据
import xlwt     #进行excel操作
import sqlite3  #进行SQLite数据库操作

findlink=r'<a href="(.*?)">'
findimgSrc=r'<img.*src="(.*?)"'
findtitle=r'<span class="title">(.*)</span>'
findRating = re.compile(r'<span class="rating_num" property="v:average">(.*)</span>')
findJudge=r'<span class="rating_num" property="v:average">(.*)</span>'
findInq=r'<span class="inq">(.*)</span>'
findBd=re.compile(r'<p class="">(.*?)</p>',re.S)

def main():
    print("start")
    baseURL="https://movie.douban.com/top250?start="
    dataList=getDate(baseURL)
    # print(dataList)
    savepath = "豆瓣电影Top250.xls"
    saveData(dataList,savepath)

def getDate(baseURL):
    dataList=[]
    for i in range(0,10):
        # print(i)
        url=baseURL + str(i*25)
        html=askURL(url)
        print(url)
        #解析数据
        soup=BeautifulSoup(html,"html.parser")
        # print(soup.find_all("div",class_="item"))

        for item in soup.find_all("div",class_="item"):
            data = []
            item=str(item)
            link=re.findall(findlink,item)[0]
            # print(link)
            data.append(link)
            imgSrc=re.findall(findimgSrc,item)[0]
            # print(imgSrc)
            data.append(imgSrc)
            titles=re.findall(findtitle,item)
            if (len(titles)>=2):
                ctitle=titles[0]
                data.append(ctitle)
                # print(ctitle)
                otitle = titles[1].replace("/","")
                data.append(otitle)
                # print(otitle)
            else:
                data.append(titles[0])
                data.append(" ")

            judgeNum = re.findall(findJudge, item)[0]
            # print(judgeNum)
            data.append(judgeNum)

            rating = re.findall(findRating,item)[0]
            data.append(rating)

            inq = re.findall(findInq, item)
            if len(inq) != 0:
                inq = inq[0].replace("。", "")  # 去掉句号
                data.append(inq)  # 添加概述
            else:
                data.append(" ")  # 留空
            # print(inq)
            bd = re.findall(findBd, item)[0]
            bd = re.sub('<br(\s+)?/>(\s+)?', " ", bd)  # 去掉<br/>
            bd = re.sub('/', " ", bd)  # 替换/
            data.append(bd.strip())  # 去掉前后的空格
            dataList.append(data)
            # print(data)i
    return dataList


def askURL(url):
    head = {
        "User-Agent": "Mozilla / 5.0(Windows NT 10.0; Win64; x64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 83.0.4103.61 Safari / 537.36"
    }
    html = ""
    request=urllib.request.Request(url,headers=head)
    try:
        response=urllib.request.urlopen(request)
        html=response.read().decode("utf-8")
        # print(html)
    except urllib.error.URLError as e:
        if hasattr(e,"code"):
            print(e.code)
        if hasattr(e,"reason"):
            print(e.reason)
    return html


def saveData(datalist,savepath):
    print("save....")
    book = xlwt.Workbook(encoding="utf-8",style_compression=0)  #创建workbook对象
    sheet = book.add_sheet('豆瓣电影Top250',cell_overwrite_ok=True)    #创建工作表
    col = ("电影详情链接","图片链接","影片中文名","影片外国名","评分","评价数","概况","相关信息")
    for i in range(0,8):
        sheet.write(0,i,col[i]) #列名
    for i in range(0,250):
        print("第%d条" %(i+1))
        data = datalist[i]
        for j in range(0,8):
            sheet.write(i+1,j,data[j])      #数据

    book.save(savepath)       #保存
















if __name__ =="__main__":
    main()
    print("爬取完成")